import http from 'http'
import io from 'socket.io'

import { SOCKET_IO_SETTINGS } from '../src/constants/socketIOConstants'

class BackendServer {
  httpServer = null
  addressInfo = null
  ioServer = null
  authenticated = null
  validToken = 'VALID_TOKEN'
  invalidToken = 'INVALID_TOKEN'

  constructor () {
    const { server, path } = SOCKET_IO_SETTINGS

    this.httpServer = http.createServer()
    this.httpServer.listen()
    this.addressInfo = this.httpServer.address()
    this.ioServer = io(this.httpServer, {
      path
    })
    this.amigoSocketNamespace = this.ioServer.of(`/${server}`)
    this.amigoSocketNamespace.on('connection', socket => {
      socket.on('authenticate', (data, callback) => {
        this.authenticated = true
        callback(true)
      })

      socket.on('disconnection', socket => {
        this.authenticated = false
      })
    })
  }

  getAddressInfo = () => {
    return this.addressInfo
  }

  emit = (eventName, data) => {
    this.amigoSocketNamespace.emit(eventName, data)
  }

  on = (eventName, callback) => {
    this.amigoSocketNamespace.on(eventName, callback)
  }

  removeAllListeners = () => {
    this.ioServer.removeAllListeners()
  }

  close = () => {
    this.authenticated = false
    this.ioServer.close()
    this.httpServer.close()
  }
}

export default BackendServer
